package main;


import main.dynamicOptimizer.DynamicOptimizer;
import main.factories.DynamicOptimizerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Orchestrates the audio manipulations
 *
 * @author Antoine Boucher, Issam Hannache
 */

public class Controller {
    String sources;
    String finaux;
    String tailles;
    String facteur;
    String sequences;


    public Controller(String sources, String finaux, String tailles, String facteur, String sequences) throws IOException {
        this.sources = sources;
        this.finaux = finaux;
        this.tailles = tailles;
        this.facteur = facteur;
        this.sequences = sequences;

        process();
    }

    /**
     *
     */
    private void process() throws IOException {
        File inputFolder = new File(sources);
        File[] listOfFiles = inputFolder.listFiles();
        File outputDir = new File(finaux);
        int[] newSize;
        double bitRateFactor;
        int nbSequences;

        // error and some validation
        if (!inputFolder.exists()) {
            throw new IOException("Input folder doesn't exist");
        }

        if (!inputFolder.isDirectory()) {
            throw new IOException("This is not a folder with sequences");
        }
        if (listOfFiles == null) {
            throw new IOException("no sequences found in source folder");
        }
        if (!outputDir.exists()) {
            if (outputDir.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                new IOException("Failed to create directory!");
            }
        }

        String[] listStrTailles = this.tailles.split(",");
        newSize = new int[listStrTailles.length];
        for (int i = 0; i < newSize.length; i++) {
            newSize[i] = Integer.parseInt(listStrTailles[i]);
        }
        bitRateFactor = Double.parseDouble(this.facteur);
        nbSequences = Integer.parseInt(this.sequences);

        //Factory pattern that create the required files
        DynamicOptimizerFactory dynamicOptimizerFactory = DynamicOptimizerFactory.getInstance();
        DynamicOptimizer dynamicOptimizer = dynamicOptimizerFactory.create(sources, finaux, newSize, bitRateFactor, nbSequences);
        // execute all the command in the chain of commands
        dynamicOptimizer.execute();

    }
}
