package main;


import main.dynamicOptimizer.analyser.ConvexEnvelope;
import main.dynamicOptimizer.analyser.SequencesAnalyserWriter;

import java.io.IOException;

/**
 * Application starting point
 *
 * @author Antoine Boucher, Issam Hannache
 */

public class Application {

    public String finaux = "";

    /**
     * Application starting point
     *
     * @param args Data from the command line. Should be: filename, method and location
     */
    public static void main(String[] args) {
        new Application(args);
    }

    /**
     * Do some basic validation on the args and call the controller
     *
     * @param args Data from the command line. Should be: filename, method and location
     */
    public Application(String[] args) {
        System.out.println("DynamicOptimizer");

        String dynamicOptimizer;
        String sources = "";
        String paramTailles;
        String tailles = "";
        String paramFacteur;
        String facteur = "";
        String paramSequences;
        String sequences = "";

        String errMsg = "dynamicOptimizer <sources> <finaux> -s <tailles> -f <facteur> -n <sequences>";

        //Get the params and some validation
        if (args.length >= 3) {
            try {
                dynamicOptimizer = args[0];
                sources = args[1];
                finaux = args[2];
                paramTailles = args[3];
                tailles = args[4];
                paramFacteur = args[5];
                facteur = args[6];
                paramSequences = args[7];
                sequences = args[8];

                if (!dynamicOptimizer.equals("dynamicOptimizer")) {
                    System.out.println(errMsg);
                    System.exit(-1);
                }
                if (!paramTailles.equals("-s")) {
                    System.out.println(errMsg);
                    System.exit(-1);
                }
                if (!paramFacteur.equals("-f")) {
                    System.out.println(errMsg);
                    System.exit(-1);
                }
                if (!paramSequences.equals("-n")) {
                    System.out.println(errMsg);
                    System.exit(-1);
                }

            } catch (Exception e) {
                System.out.println(errMsg);
                System.exit(-1);
            }

            try {
                Controller controller = new Controller(sources, finaux, tailles, facteur, sequences);
            } catch (IOException e) {
                System.out.println(errMsg);
                System.exit(-1);
            }
            System.out.println("Done");

        } else {
            System.out.println(errMsg);
            System.exit(-1);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                System.out.println("Shutdown Hook is running !");
                ConvexEnvelope.getInstance().calculateConvexEnvelope();
                ConvexEnvelope.getInstance().writeIntoAnalyserLog();
                if (!finaux.equals("")) {
                    SequencesAnalyserWriter.getInstance().write(finaux + "SequencesAnalyser.txt");
                } else {
                    SequencesAnalyserWriter.getInstance().write("SequencesAnalyser.txt");
                }

            }
        });

    }

}