package main.dynamicOptimizer;

public class DynamicOptimizer implements ICommand {

    SequenceDynamicOptimizer[] sequenceDynamicOptimizers;

    public DynamicOptimizer(SequenceDynamicOptimizer[] sequenceDynamicOptimizers) {
        this.sequenceDynamicOptimizers = sequenceDynamicOptimizers;
    }

    /**
     * Execute all the sequencesDynamicOptimizers
     */
    @Override
    public void execute() {
        for (SequenceDynamicOptimizer sDyOp : sequenceDynamicOptimizers) {
            sDyOp.execute();
        }
    }
}
