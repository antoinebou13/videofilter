package main.dynamicOptimizer;

public interface ICommand {
    /**
     * command pattern
     */
    void execute();
}
