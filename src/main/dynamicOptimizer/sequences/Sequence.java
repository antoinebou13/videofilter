package main.dynamicOptimizer.sequences;

import java.io.File;

public class Sequence extends AbstractSequence {

    public Sequence(String fileName) {
        super(fileName);
    }

    @Override
    public boolean validate() {
        if(!new File(getFilePath()).exists()){
            return false;
        }
        return true;
    }

}
