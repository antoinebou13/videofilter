package main.dynamicOptimizer.sequences;

import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.VideoInfo;

import java.io.File;

public abstract class AbstractSequence {

    private String filePath;

    public AbstractSequence(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return new File(filePath).getName();
    }

    public String getFilePath() {
        return filePath;
    }

    public abstract boolean validate();

    public VideoInfo getVideoInfo() {
        try {
            return getMultimediaObject().getInfo().getVideo();
        } catch (EncoderException e) {
            e.printStackTrace();
        }
        return null;
    }

    public MultimediaObject getMultimediaObject() {
        return new MultimediaObject(new File(filePath));
    }

    public int[] getSize() {
        return new int[]{getVideoInfo().getSize().getWidth(), getVideoInfo().getSize().getHeight()};
    }

    public double getBitRate() {
        return getVideoInfo().getBitRate();
    }

    public String getVideoCodec() {
        return getVideoInfo().getDecoder();
    }

    public String getExtension() {
        String extension = "";
        int index = getFileName().lastIndexOf(".");

        if (index > 0) {
            extension = getFileName().substring(index + 1);
        }

        return extension;
    }

}
