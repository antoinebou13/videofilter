package main.dynamicOptimizer.sequences;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestSequence extends AbstractSequence {

    private int newBitRate;

    public TestSequence(String filePath) {
        super(filePath);
    }

    @Override
    public boolean validate() {
        if(!new File(getFilePath()).exists()){
            return false;
        }
        Pattern p = Pattern.compile(".*?(_)(\\\\d+)(_)(\\\\d+)(_)(\\\\d+)(\\\\.)",Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(getFilePath());
        if (!m.find()){
            return false;
        }

        return true;
    }

    public void setNewBitRate(int bitRate) {
        newBitRate = bitRate;
    }

    public int getNewBitRate() {
        return newBitRate;
    }
}
