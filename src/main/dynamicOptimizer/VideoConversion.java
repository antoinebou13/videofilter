package main.dynamicOptimizer;

import main.dynamicOptimizer.sequences.AbstractSequence;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import ws.schild.jave.*;

import java.io.File;

public class VideoConversion {

    private AbstractSequence sequence;
    private VideoAttributes vA;
    private String outputDir;

    public VideoConversion(AbstractSequence sequence, String outputDir) {
        this.sequence = sequence;
        this.outputDir = outputDir;

        this.vA = new VideoAttributes();
    }

    public int calculateNewBitRate(int oldBitRate, double bitRateFactor) {
        return (int) (oldBitRate * (bitRateFactor));
    }

    public String generateOutputFilePath(String name, int h, int w, int bitRate, String ext) {
        return outputDir + name + "_" + h + "_" + w + "_" + bitRate + "." + ext;
    }

    /**
     * calculate new videosize based on height
     * @param h the height
     * @param enableCorrectRatio enable 4/3, 16/9 ratio
     * @return VideoSize object with new Size
     */
    public VideoSize calculateVideoSize(int h, boolean enableCorrectRatio) {
        double ratio;
        if (enableCorrectRatio) {
            if (h <= 480) {
                ratio = (double) 4 / 3;
            } else if (h > 480 && h <= 1080) {
                ratio = (double) 16 / 9;
            } else {
                ratio = ((double) sequence.getSize()[0] / sequence.getSize()[1]);
            }
        } else {
            ratio = ((double) sequence.getSize()[0] / sequence.getSize()[1]);
        }

        return new VideoSize(((int) Math.ceil(ratio * h)), h);
    }

    public VideoAttributes resizeVideo(VideoAttributes va, int w, int h) {
        va.setQuality(1);
        va.setSize(new VideoSize(w, h));
        return va;
    }

    public VideoAttributes changeBitRate(VideoAttributes va, int bitRate) {
        va.setBitRate(bitRate);
        return va;
    }

    public EncodingAttributes encoding(VideoAttributes va) {
        EncodingAttributes attrs = new EncodingAttributes();
        attrs.setVideoAttributes(va);
        return attrs;
    }

    public void encode(EncodingAttributes ea, String outputFile) {
        Encoder encoder = new Encoder();
        try {
            encoder.encode(new MultimediaObject(new File(sequence.getFilePath())), new File(outputFile), ea, new ProgressBar());
        } catch (EncoderException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert the video to the original size for psnr calculation
     * @param va
     * @param newSequence
     * @return
     */
    public TestSequence convertOriginalVideoSize(VideoAttributes va, Sequence newSequence) {
        TestSequence newTestSequence;
        va = resizeVideo(va, newSequence.getSize()[0], newSequence.getSize()[1]);
        encode(encoding(va), outputDir + "tmp_" + sequence.getFileName());
        newTestSequence = new TestSequence(outputDir + "tmp_" + sequence.getFileName());
        return newTestSequence;
    }

    public boolean deleteTmpVideo(String filePath) {
        try {
            File f = new File(filePath);
            if (f.delete()) {
                System.out.println(f.getName() + " deleted");
                return true;
            } else {
                System.out.println("failed to delete");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
