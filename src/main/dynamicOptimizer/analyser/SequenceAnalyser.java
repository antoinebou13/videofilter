package main.dynamicOptimizer.analyser;

import main.dynamicOptimizer.ICommand;
import main.dynamicOptimizer.VideoConversion;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import ws.schild.jave.VideoAttributes;

public class SequenceAnalyser implements ICommand {

    private TestSequence[] sequencesTests;
    private Sequence sequenceOriginal;
    private String outputDir;
    private SequencesAnalyserWriter sequencesAnalyserWriter = SequencesAnalyserWriter.getInstance();


    public SequenceAnalyser(Sequence sequenceOriginal, TestSequence[] sequencesTests, String outputDir) {
        this.sequencesTests = sequencesTests;
        this.sequenceOriginal = sequenceOriginal;
        this.outputDir = outputDir;
    }

    /**
     * Create tmp file to ompare with sequence original for psnr
     */
    public void analyseTestSequences() {
        for (int i = 0; i < sequencesTests.length; i++) {
            VideoConversion vC = new VideoConversion(sequencesTests[i], outputDir);
            TestSequence tmpTestSequence = vC.convertOriginalVideoSize(new VideoAttributes(), sequenceOriginal);
            PSNR psnr = new PSNR(sequenceOriginal, tmpTestSequence);
            psnr.execute();
            vC.deleteTmpVideo(tmpTestSequence.getFilePath());
            SequencesAnalyserWriter.getInstance().createAnalyserText(
                    sequencesTests[i].getFileName(),
                    sequenceOriginal.getBitRate(),
                    psnr.getPSNR()
            );
            ConvexEnvelope.getInstance().addBitRate(sequencesTests[i].getNewBitRate());
            ConvexEnvelope.getInstance().addPSNR(psnr.getPSNR());
        }
    }

    @Override
    public void execute() {
        analyseTestSequences();
    }

    public SequencesAnalyserWriter getSequencesAnalyserWriter() {
        return sequencesAnalyserWriter;
    }
}
