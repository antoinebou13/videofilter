package main.dynamicOptimizer.analyser;

import java.util.ArrayList;
import java.util.List;

public class ConvexEnvelope {

    private static ConvexEnvelope instance;
    private ArrayList<Double> psnrs = new ArrayList<>();
    private ArrayList<Integer> bitRates = new ArrayList<>();
    private List<PointHull> list;

    private ConvexEnvelope() {
    }


    public boolean addPSNR(double psnr) {
        return this.psnrs.add(psnr);
    }

    public static ConvexEnvelope getInstance() {
        if (instance == null) {
            instance = new ConvexEnvelope();
        }
        return instance;
    }

    /**
     * Calculate the ConvexEnvelope/Hull
     */
    public void calculateConvexEnvelope() {
        List<PointHull> pointHulls = new ArrayList<PointHull>();
        for (int i = 0; i < psnrs.size(); i++) {
            pointHulls.add(new PointHull(bitRates.get(i), psnrs.get(i)));
        }
        list = ConvexHull.makeHull(pointHulls);
    }

    /**
     * Write the Convex hull in the logs
     */
    public void writeIntoAnalyserLog() {
        SequencesAnalyserWriter.getInstance().addToBuffer(new String[]{list.toString()});
    }


    public boolean addBitRate(int bitRate) {
        return this.bitRates.add(bitRate);
    }
}
