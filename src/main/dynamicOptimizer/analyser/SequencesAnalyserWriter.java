package main.dynamicOptimizer.analyser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SequencesAnalyserWriter {

    private static SequencesAnalyserWriter instance;
    private BufferedWriter bufferedWriter;
    private ArrayList<String> buffer = new ArrayList<>();

    /**
     * Save logs in a buffer
     * @param lines all the lines to save in buffer
     * @return
     */
    public boolean addToBuffer(String[] lines) {
        boolean added = false;
        for (String line : lines) {
            added = buffer.add(line);
            if (added == false) {
                return added;
            }
        }
        return added;
    }

    /**
     * Write logs in the shutdown hook
     * @param outputFile
     */
    public void write(String outputFile) {
        System.out.println("Writing in " + outputFile);
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(outputFile, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (buffer.size() > 0) {
            for (String line : buffer) {
                try {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param name The name of the test sequence
     * @param bitRate the original bitRate
     * @param PSNR The psnr
     * @return function executed correctly == true
     */
    public boolean createAnalyserText(String name, double bitRate, double PSNR) {
        String sequenceName = "Sequence Name: " + name;
        String sequenceBitRate = "Bit Rate: " + bitRate;
        String sequencePSNR = "PSNR: " + PSNR;
        return addToBuffer(new String[]{sequenceName, sequenceBitRate, sequencePSNR});
    }

    public static SequencesAnalyserWriter getInstance() {
        if (instance == null) {
            instance = new SequencesAnalyserWriter();
        }
        return instance;
    }

    public ArrayList<String> getBuffer() {
        return buffer;
    }
}
