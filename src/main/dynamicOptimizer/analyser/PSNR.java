package main.dynamicOptimizer.analyser;

import main.dynamicOptimizer.ICommand;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import ws.schild.jave.DefaultFFMPEGLocator;
import ws.schild.jave.FFMPEGExecutor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PSNR implements ICommand {
    Sequence seqOriginal;
    TestSequence seqTest;
    FFMPEGExecutor ffmpeg = new DefaultFFMPEGLocator().createExecutor();

    double psnr;

    public PSNR(Sequence seqOriginal, TestSequence seqTest) {
        this.seqOriginal = seqOriginal;
        this.seqTest = seqTest;
    }

    @Override
    public void execute() {
        calculPSNR();
    }

    /**
     * https://github.com/stoyanovgeorge/ffmpeg/wiki/How-to-Compare-Video
     * @return PSNR for the comparision of sequence and test sequence
     */
    public double calculPSNR() {
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument(seqOriginal.getFilePath());
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument(seqTest.getFilePath());
        ffmpeg.addArgument("-lavfi");
        ffmpeg.addArgument("psnr");
        ffmpeg.addArgument("-f");
        ffmpeg.addArgument("null");
        ffmpeg.addArgument("-");

        try {
            ffmpeg.execute();
        } catch (IOException e) {
            e.printStackTrace();
            ffmpeg.destroy();
        }

        readPSNR();
        return psnr;
    }

    /**
     * read PSNR for the FFMPEGExecutor console stream and parse only the psnr
     */
    public void readPSNR() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getErrorStream()));
            int lineNR = 0;

            String line;
            while ((line = reader.readLine()) != null) {
                if (line.contains("PSNR")) {
                    String PSNRString = line.split("PSNR")[1].split("average:")[1].split("min:")[0].trim();
                    if (PSNRString.equals("inf") && !PSNRString.equals("")) {
                        psnr = Double.POSITIVE_INFINITY;
                    } else if (!PSNRString.equals("inf") && !PSNRString.equals("")) {
                        psnr = Double.parseDouble(PSNRString);
                    } else {
                        System.out.println("Error while calculating the PSNR");
                        psnr = -1;
                    }
                }
                lineNR++;
            }

            if (psnr == 0.0) {
                System.out.println("Error while calculating the PSNR");
                psnr = -1;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ffmpeg.destroy();
        }
    }

    public double getPSNR() {
        return psnr;
    }

}
