package main.dynamicOptimizer;

import ws.schild.jave.EncoderProgressListener;
import ws.schild.jave.MultimediaInfo;

public class ProgressBar implements EncoderProgressListener {

    private int progressStatus;
    private char[] animationChars = new char[]{'|', '/', '-', '\\'};

    @Override
    public void sourceInfo(MultimediaInfo multimediaInfo) {
//        System.out.println(multimediaInfo.getVideo());
    }

    @Override
    public void progress(int i) {
        this.progressStatus = i;
        System.out.println("Processing: " + Math.round(((double) i / 1000) * 100) + "% . " + animationChars[i % 4] + "\r");
    }

    @Override
    public void message(String s) {
//        System.out.println(s);
    }
}
