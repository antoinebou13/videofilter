package main.factories;

import main.dynamicOptimizer.DynamicOptimizer;
import main.dynamicOptimizer.SequenceDynamicOptimizer;
import main.factories.builder.SequenceDynamicOptimizerBuilder;

import java.io.File;

public class DynamicOptimizerFactory {

    private static DynamicOptimizerFactory instance;
    private SequenceDynamicOptimizer[] sequenceDynamicOptimizers;

    private DynamicOptimizerFactory() {
    }

    /**
     * @param sourceDir     source directory of all sequences
     * @param outputDir     output directory of all testsequences
     * @param newSize       the list of new height size for the testsequences
     * @param bitRateFactor the bitratefactor to reduce each iteration
     * @param nbSequences   the number of testsequences per sequence
     * @return
     */
    public DynamicOptimizer create(String sourceDir, String outputDir, int[] newSize, double bitRateFactor, int nbSequences) {
        int i = 0;
        SequenceDynamicOptimizerBuilder seqDyOpBuilder;
        sequenceDynamicOptimizers = new SequenceDynamicOptimizer[allFilesInDir(sourceDir).length];
        for (String file : allFilesInDir(sourceDir)) {
            seqDyOpBuilder = new SequenceDynamicOptimizerBuilder();
            seqDyOpBuilder.setFilePath(sourceDir + file);
            seqDyOpBuilder.setNewSize(newSize);
            seqDyOpBuilder.setBitRateFactor(bitRateFactor);
            seqDyOpBuilder.setNbTestSequences(nbSequences);
            seqDyOpBuilder.setOutputDir(outputDir);
            sequenceDynamicOptimizers[i] = seqDyOpBuilder.build();
            i++;
        }
        return new DynamicOptimizer(sequenceDynamicOptimizers);
    }

    /**
     * https://stackoverflow.com/questions/5694385/getting-the-filenames-of-all-files-in-a-folder
     *
     * @param sourceDir source directory of all sequences
     * @return list of string file path
     */
    public String[] allFilesInDir(String sourceDir) {
        File folder = new File(sourceDir);
        File[] listOfFiles = folder.listFiles();
        String[] fileNames = new String[listOfFiles.length];

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                fileNames[i] = listOfFiles[i].getName();
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        return fileNames;
    }

    public static DynamicOptimizerFactory getInstance() {
        if (instance == null) {
            instance = new DynamicOptimizerFactory();
        }
        return instance;
    }
}
