package main.factories.builder;

import main.dynamicOptimizer.SequenceDynamicOptimizer;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;

public class SequenceDynamicOptimizerBuilder implements IBuilder<SequenceDynamicOptimizer> {

    private String filePath;
    private String outputDir;
    private int[] newSize;
    private double bitRateFactor;
    private int nbSequences;

    private Sequence sequence;
    private TestSequence[][] testSequences;
    SequenceDynamicOptimizer sequenceDynamicOptimizer;

    public SequenceDynamicOptimizerBuilder() {
    }

    public SequenceDynamicOptimizerBuilder(String filePath, String outputDir, int[] newSize, int bitRateFactor, int nbSequences) {
        this.filePath = filePath;
        this.outputDir = outputDir;
        this.newSize = newSize;
        this.bitRateFactor = bitRateFactor;
        this.nbSequences = nbSequences;
    }

    @Override
    public SequenceDynamicOptimizer build() {
        createSequence();
        createTestSequences();
        sequenceDynamicOptimizer = new SequenceDynamicOptimizer(outputDir, newSize, nbSequences, sequence, testSequences);
        return sequenceDynamicOptimizer;

    }

    /**
     * Create Sequence object for file path
     *
     * @return sequence instance based on attribut filePath
     */
    public Sequence createSequence() {
        sequence = new Sequence(filePath);
        if (sequence.validate()){
            System.out.println("Error in Sequence object or file");
        }
        return this.sequence;
    }

    /**
     * Create TestSequence from builder pattern
     *
     * @return testsequences based on attributs
     */
    public TestSequence[][] createTestSequences() {
        testSequences = new TestSequencesBuilder(sequence, outputDir, newSize, bitRateFactor, nbSequences).build();
        return testSequences;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public int[] getNewSize() {
        return newSize;
    }

    public void setNewSize(int[] newSize) {
        this.newSize = newSize;
    }

    public double getBitRateFactor() {
        return bitRateFactor;
    }

    public void setBitRateFactor(double bitRateFactor) {
        this.bitRateFactor = bitRateFactor;
    }

    public int getNbSequences() {
        return nbSequences;
    }

    public void setNbTestSequences(int nbTestSequences) {
        this.nbSequences = nbTestSequences;
    }
}
