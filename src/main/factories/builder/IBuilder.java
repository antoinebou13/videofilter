package main.factories.builder;

public interface IBuilder<T> {

    T build();
}
