package main.factories.builder;

import main.dynamicOptimizer.VideoConversion;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import ws.schild.jave.VideoAttributes;

public class TestSequencesBuilder implements IBuilder<TestSequence[][]> {

    private Sequence sequence;
    private String outputDir;
    private int[] newSize;
    private double bitRateFactor;
    private int nbTestSequences;

    public TestSequencesBuilder(Sequence sequence, String outputDir, int[] newSize, double bitRateFactor, int nbTestSequences) {
        this.sequence = sequence;
        this.outputDir = outputDir;
        this.newSize = newSize;
        this.bitRateFactor = bitRateFactor;
        this.nbTestSequences = nbTestSequences;
    }

    @Override
    public TestSequence[][] build() {
        TestSequence[][] testSequences = new TestSequence[newSize.length][nbTestSequences];
        VideoConversion vC;
        VideoAttributes vA = new VideoAttributes();
        int count = 0;
        int width;
        int height;
        int oldBitRate = (int) sequence.getBitRate();
        int newBitRate;
        String outputFile;
        String[] fileNameSplit = sequence.getFileName().split("\\.");


        for (int i = 0; i < newSize.length; i++) {
            oldBitRate = (int) sequence.getBitRate(); // reset bitRate to last iteration value
            for (int j = 0; j < nbTestSequences; j++) {
                ++count;

                vC = new VideoConversion(sequence, outputDir);

                width = vC.calculateVideoSize(newSize[i], false).getWidth();
                height = newSize[i];
                newBitRate = vC.calculateNewBitRate(oldBitRate, bitRateFactor);
                oldBitRate = newBitRate;
                outputFile = vC.generateOutputFilePath(fileNameSplit[0], height, width, newBitRate, sequence.getExtension());

                System.out.println(sequence.getFileName() + ": " + outputFile + " " + count);

                vA = vC.resizeVideo(vA, width, height);
                vA = vC.changeBitRate(vA, newBitRate);
                vC.encode(vC.encoding(vA), outputFile);
                TestSequence testSequence = new TestSequence(outputFile);
                testSequence.setNewBitRate(newBitRate);
                if (!testSequence.validate()){
                    System.out.println("Error in testSequences object or file");
                }
                testSequences[i][j] = testSequence;

            }
        }
        return testSequences;
    }


}
