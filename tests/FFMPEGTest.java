import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ws.schild.jave.DefaultFFMPEGLocator;
import ws.schild.jave.FFMPEGExecutor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class FFMPEGTest {
    FFMPEGExecutor ffmpeg;

    @BeforeEach
    void setUp() {
        ffmpeg = new DefaultFFMPEGLocator().createExecutor();
    }

    @AfterEach
    void tearDown(){
        try {
            File f= new File("tests/resources/BirdNoSound_test.avi");
            if(f.delete()){
                System.out.println(f.getName() + " deleted");
            } else {
                System.out.println("failed");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void test() {
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument("tests/resources/BirdNoSound.mp4");
        ffmpeg.addArgument("tests/resources/BirdNoSound_test.avi");
        try {
            ffmpeg.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getErrorStream()));
            int lineNR = 0;

            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
                lineNR++;
            }
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } finally {
            ffmpeg.destroy();
        }

        if (new File("tests/resources/BirdNoSound_test.avi").exists()){
            assertTrue(true);
        }else{
            assertTrue(false);
        }

    }

}