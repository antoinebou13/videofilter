package main.factories;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DynamicOptimizerFactoryTest {

    DynamicOptimizerFactory dynamicOptimizerFactory;

    @BeforeEach
    void setUp() {
        dynamicOptimizerFactory = DynamicOptimizerFactory.getInstance();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void create() {
        try {
            dynamicOptimizerFactory.create("tests/resources/sequences/", "tests/test_output/", new int[]{360, 480}, 50, 3);
            assertTrue(true);
        } catch (Exception e){
            assertTrue(false);
        }
    }

    @Test
    void createFail() {
        try {
            dynamicOptimizerFactory.create("tests/resourcesss/", "tests/test_output/", new int[]{360}, 50, 3);
            assertTrue(false);
        } catch (Exception e){
            assertTrue(true);
        }
    }

    @Test
    void allFilesInDir() {
        assertArrayEquals(dynamicOptimizerFactory.allFilesInDir("tests/resources/sequences/"), new String[]{"video3_s1.mp4", "video3_s2.mp4"});
    }

    @Test
    void getInstance() {
        assertTrue(DynamicOptimizerFactory.getInstance() instanceof DynamicOptimizerFactory);
    }
}