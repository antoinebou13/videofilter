package main;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.security.Permission;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    Application app;
    Application app2;


    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
        System.setSecurityManager(null);

        File f = new File("tests/test_output/");
        for (File file : f.listFiles()) {
            try {
                if (file.delete()) {
                    System.out.println(f.getName() + " deleted");
                    assertTrue(true);
                } else {
                    System.out.println("failed to delete");
                    assertTrue(true);
                }
            } catch(Exception e){
                e.printStackTrace();
                assertTrue(false);
            }
        }
    }

    @Test
    void main() {
        app.main(new String[]{"dynamicOptimizer", "tests/resources/sequences/", "tests/test_output/", "-s", "360", "-f", "0.5", "-n", "1"});
        String outPrint = outContent.toString();
        String[] outLines = outPrint.split("\\r?\\n");
        assertEquals(outLines[0],"DynamicOptimizer");
        assertEquals(outLines[outLines.length-1],"Done");

        System.setOut(originalOut);
        System.setOut(new PrintStream(outContent));
    }
    @Test
    void mainFailed(){
        SecurityManager sM = new NoExitSecurityManager();
        System.setSecurityManager(sM);

        try{
            app2.main(new String[]{"dynamicOptimizer",  "tests/resources/sequences/"});
        } catch (SecurityException e){
            String outPrint = outContent.toString();
            String[] outLines = outPrint.split("\\r?\\n");
            assertEquals(outLines[0], "DynamicOptimizer");
            assertEquals(outLines[1], "dynamicOptimizer <sources> <finaux> -s <tailles> -f <facteur> -n <sequences>");
        }
    }

    public static class NoExitSecurityManager extends SecurityManager {

        @Override
        public void checkPermission(Permission perm) {}

        @Override
        public void checkPermission(Permission perm, Object context) {}

        @Override
        public void checkExit(int status) {
            super.checkExit(status);
            throw new SecurityException();
        }
    }



}