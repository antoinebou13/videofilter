package main;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.security.Permission;

import static org.junit.jupiter.api.Assertions.*;

class ControllerTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    Controller controller;
    Controller controller1;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void tearDown() {
        System.setOut(originalOut);
        System.setSecurityManager(null);

        File f = new File("tests/test_output/");
        for (File file : f.listFiles()) {
            try {
                if (file.delete()) {
                    System.out.println(f.getName() + " deleted");
                    assertTrue(true);
                } else {
                    System.out.println("failed to delete");
                    assertTrue(true);
                }
            } catch(Exception e){
                e.printStackTrace();
                assertTrue(false);
            }
        }
    }

    @Test
    void process(){
        try {
            controller = new Controller("tests/resources/sequences/", "tests/test_output/",  "360,480",  "0.5",  "2");
        } catch (IOException e) {
            assertTrue(false);
            e.printStackTrace();
        }
        assertTrue(true);
    }

    @Test
    void processFail(){
        SecurityManager sM = new NoExitSecurityManager();
        System.setSecurityManager(sM);

        try{
            controller1 = new Controller("dasda/", "tests/test_output/",  "360,480",  "0.5",  "2");
            assertTrue(false);
        } catch (SecurityException se){
            assertTrue(true);
            String outPrint = outContent.toString();
            String[] outLines = outPrint.split("\\r?\\n");
            assertEquals(outLines[0], "DynamicOptimiser");
        } catch (IOException e) {
            assertTrue(true);
        }

    }

    public static class NoExitSecurityManager extends SecurityManager {

        @Override
        public void checkPermission(Permission perm) {}

        @Override
        public void checkPermission(Permission perm, Object context) {}

        @Override
        public void checkExit(int status) {
            super.checkExit(status);
            throw new SecurityException();
        }
    }

}