package main.dynamicOptimizer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VideoConversionTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void calculateNewBitRate() {
    }

    @Test
    void generateOutputFilePath() {
    }

    @Test
    void calculateVideoSize() {
    }

    @Test
    void resizeVideo() {
    }

    @Test
    void changeBitRate() {
    }

    @Test
    void encoding() {
    }

    @Test
    void encode() {
    }

    @Test
    void convertOriginalVideoSize() {
    }

    @Test
    void deleteTmpVideo() {
    }
}