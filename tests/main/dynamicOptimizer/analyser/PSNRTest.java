package main.dynamicOptimizer.analyser;

import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ws.schild.jave.DefaultFFMPEGLocator;
import ws.schild.jave.FFMPEGExecutor;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class PSNRTest {
    FFMPEGExecutor ffmpeg;
    FFMPEGExecutor ffmpeg1;

    Sequence sequence;
    TestSequence testSequence;
    PSNR psnr;

    Sequence sequence1;
    TestSequence testSequence1;
    PSNR psnr1;

    @BeforeEach
    void setUp() {
        ffmpeg = new DefaultFFMPEGLocator().createExecutor();
        ffmpeg1 = new DefaultFFMPEGLocator().createExecutor();
        sequence = new Sequence("tests/resources/BirdNoSound.mp4");
        testSequence = new TestSequence("tests/resources/BirdNoSound.mp4");
        psnr = new PSNR(sequence, testSequence);

        sequence1 = new Sequence("tests/resources/sequences/video3_s1.mp4");
        testSequence1 = new TestSequence("tests/resources/sequences/video3_s2.mp4");
        psnr1 = new PSNR(sequence1, testSequence1);
    }

    @Test
    void calculPSNR(){
        psnr.execute();
        assertEquals(Double.POSITIVE_INFINITY, psnr.getPSNR());

        psnr1.execute();
        assertEquals(10.819717, psnr1.getPSNR());
    }

    @Test
    void calculPSNRFFMPEGExecutorSame() {
        // https://github.com/stoyanovgeorge/ffmpeg/wiki/How-to-Compare-Video
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument("tests/resources/BirdNoSound.mp4");
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument("tests/resources/BirdNoSound.mp4");
        ffmpeg.addArgument("-lavfi");
        ffmpeg.addArgument("psnr");
        ffmpeg.addArgument("-f");
        ffmpeg.addArgument("null");
        ffmpeg.addArgument("-");

        try {
            ffmpeg.execute();
        } catch (IOException e) {
            e.printStackTrace();
            ffmpeg.destroy();
            assertTrue(false);
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getErrorStream()));
            int lineNR = 0;
            boolean foundPSNR = false;

            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
                if (line.contains("PSNR")){
                    foundPSNR = true;
                    assertEquals("y:inf u:inf v:inf average:inf min:inf max:inf", line.split("PSNR")[1].trim());
                    assertEquals("inf", line.split("PSNR")[1].split("average:")[1].split("min:")[0].trim());
                }
                lineNR++;
            }
            assertTrue(foundPSNR);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } finally {
            ffmpeg.destroy();
        }
    }

    @Test
    void calculPSNRFFMPEGExecutor() {
        // https://github.com/stoyanovgeorge/ffmpeg/wiki/How-to-Compare-Video
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument("tests/resources/sequences/video3_s1.mp4");
        ffmpeg.addArgument("-i");
        ffmpeg.addArgument("tests/resources/sequences/video3_s2.mp4");
        ffmpeg.addArgument("-filter_complex");
        ffmpeg.addArgument("psnr");
        ffmpeg.addArgument("-f");
        ffmpeg.addArgument("null");
        ffmpeg.addArgument("-");

        try {
            ffmpeg.execute();
        } catch (IOException e) {
            e.printStackTrace();
            ffmpeg.destroy();
            assertTrue(false);
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(ffmpeg.getErrorStream()));
            int lineNR = 0;
            boolean foundPSNR = false;

            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line);
                if (line.contains("PSNR")){
                    foundPSNR = true;
                    assertEquals(10.819717, Double.parseDouble(line.split("PSNR")[1].split("average:")[1].split("min:")[0].trim()));
                }
                lineNR++;
            }
            assertTrue(foundPSNR);
        } catch (IOException e) {
            e.printStackTrace();
            assertTrue(false);
        } finally {
            ffmpeg.destroy();
        }
    }

}