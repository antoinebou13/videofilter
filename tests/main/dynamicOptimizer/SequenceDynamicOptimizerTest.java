package main.dynamicOptimizer;

import main.dynamicOptimizer.analyser.SequenceAnalyser;
import main.dynamicOptimizer.sequences.Sequence;
import main.dynamicOptimizer.sequences.TestSequence;
import main.factories.builder.TestSequencesBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SequenceDynamicOptimizerTest {

    String outputDir = "tests/test_output/";
    TestSequencesBuilder testSequencesBuilder;
    TestSequence[][] testSequences;
    Sequence sequence;
    SequenceDynamicOptimizer sequenceDynamicOptimizer;

    @BeforeEach
    void setUp() {
        sequence = new Sequence("tests/resources/sequences/video3_s1.mp4");
        testSequencesBuilder = new TestSequencesBuilder(sequence, outputDir, new int[]{360, 480}, 50, 3);
        testSequences = testSequencesBuilder.build();
        sequenceDynamicOptimizer = new SequenceDynamicOptimizer(
                outputDir,
                new int[]{360, 480},
                3,
                sequence,
                testSequences
        );
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void execute() {
        sequenceDynamicOptimizer.execute();
        SequenceAnalyser sequenceAnalyser = new SequenceAnalyser(sequence, sequenceDynamicOptimizer.flatting2DTestSequencesArray(testSequences), outputDir);
        assertTrue(sequenceDynamicOptimizer.getSequencesAnalyser() instanceof SequenceAnalyser);
        assertTrue(sequenceDynamicOptimizer.getSequencesAnalyser().getSequencesAnalyserWriter().getBuffer().size() != 0);

    }

    @Test
    void flatting2DTestSequencesArray() {
        assertTrue(sequenceDynamicOptimizer.flatting2DTestSequencesArray(testSequences) instanceof TestSequence[]);
    }
}