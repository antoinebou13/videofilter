package main.dynamicOptimizer.sequences;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AbstractSequenceTest {

    Sequence sequence;
    TestSequence testSequence;

    @BeforeEach
    void setUp() {
        sequence = new Sequence("tests/resources/sequences/video3_s1.mp4");
        testSequence = new TestSequence("tests/resources/test_sequences/");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getFileName() {
        assertEquals(sequence.getFileName(), "video3_s1.mp4");
    }

    @Test
    void getFilePath() {
        assertEquals(sequence.getFilePath(), "tests/resources/sequences/video3_s1.mp4");
    }

    @Test
    void validate() {
    }

    @Test
    void getVideoInfo() {
        assertEquals(sequence.getFilePath(), "tests/resources/sequences/video3_s1.mp4");
    }

    @Test
    void getMultimediaObject() {
        assertEquals(sequence.getFilePath(), "tests/resources/sequences/video3_s1.mp4");
    }

    @Test
    void getSize() {
        assertEquals(sequence.getSize()[0], 1920);
        assertEquals(sequence.getSize()[1], 1080);
    }

    @Test
    void getBitRate() {
        assertEquals(sequence.getBitRate(), 6917000.0);
    }

    @Test
    void getVideoCodec() {
        assertEquals(sequence.getVideoCodec(), "h264 (High) (avc1 / 0x31637661)");
    }

    @Test
    void getExtension() {
        assertEquals(sequence.getExtension(), "mp4");
    }
}